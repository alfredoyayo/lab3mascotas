package com.example.amlab3mascota

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.amlab3mascota.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        events()
        supportActionBar?.hide()
    }

    private fun events() {
        binding.btnSend.setOnClickListener { sendMascotData() }
    }

    private fun sendMascotData() {
        val intent = Intent(this,DestinationActivity::class.java);
        binding.apply {
            val name = edtName.text.toString().trim()
            val age = edtAge.text.toString().trim()

            if (name.isEmpty()) { Toast.makeText(this@MainActivity, "El nombre es requerido", Toast.LENGTH_SHORT) .show(); return }
            if (age.isEmpty()) { Toast.makeText(this@MainActivity, "La edad es requerida", Toast.LENGTH_SHORT) .show(); return }

            val mascot = if (rbCat.isChecked) "Gato" else { if (rbDog.isChecked) "Perro" else "Conejo"}

            val vaccinesList = mutableListOf<String>()
            if (chkVaccine1.isChecked) vaccinesList.add(chkVaccine1.text.toString())
            if (chkVaccine2.isChecked) vaccinesList.add(chkVaccine2.text.toString())
            if (chkVaccine3.isChecked) vaccinesList.add(chkVaccine3.text.toString())
            if (chkVaccine4.isChecked) vaccinesList.add(chkVaccine4.text.toString())
            if (chkVaccine5.isChecked) vaccinesList.add(chkVaccine5.text.toString())
            val vaccines = if (vaccinesList.size>0)vaccinesList.joinToString()else "Ninguna"

            intent.putExtra("name",name)
            intent.putExtra("age",age)
            intent.putExtra("mascot",mascot)
            intent.putExtra("vaccines",vaccines)
        }
        startActivity(intent)
    }
}