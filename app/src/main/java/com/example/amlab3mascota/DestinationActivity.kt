package com.example.amlab3mascota

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.amlab3mascota.databinding.ActivityDestinationBinding

class DestinationActivity : AppCompatActivity() {
    lateinit var binding: ActivityDestinationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDestinationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getValuesFromBundle()
        supportActionBar?.title = "Informacion de mascota"
    }

    private fun getValuesFromBundle() {
        intent.extras?.let {
            it.apply {
                binding.apply {
                    tvName.text = getString("name","") ?: ""
                    tvAge.text = getString("age","") ?: ""
                    val mascot =getString("mascot","") ?: ""
                    tvMascot.text = mascot
                    when (mascot) {
                        "Gato" -> ivMascot.setImageResource(R.drawable.gato)
                        "Perro" -> ivMascot.setImageResource(R.drawable.perro)
                        else -> ivMascot.setImageResource(R.drawable.conejo)
                    }
                    tvVaccine.text = getString("vaccines","") ?: ""
                }
            }
        }
    }
}